// Filename: Guessing_Game.c
#include <stdio.h>


int main(void)
{
  int a;
  puts("This is a guessing game.\nThe rule are simple:just try to guess the number: ");
  scanf("%d", &a);
  
  while (a != 386) {

      //Decided to use conditional operator than a if..esle statement
      
    a < 386 ? puts("Too low") : puts("Too high");
    scanf("%d", &a);
  }

 puts("Congratulation, you actually did it XD");
 
}
